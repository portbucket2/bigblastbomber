﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeProfile : ScriptableObject
{
    public UpgradeType type;
    public string title;
    public float valueInitial = 100;
    public float valueGainPerLevel = 10;
    public int costInitial = 10;
    public float costGainRatioPerLevel = 1.2f;

    public int currentLevel { get { return PDATA.GetPowerUpLevel(type); } }

    public int CurrentValue
    {
        get
        {
            return Mathf.RoundToInt(valueInitial + (currentLevel) * valueGainPerLevel);
        }
    }
    public int UpgradeGain
    {
        get
        {
            return Mathf.RoundToInt(valueGainPerLevel);
        }
    }
    public int UpgradeCost
    {
        get
        {
            return Mathf.RoundToInt(costInitial * Mathf.Pow(costGainRatioPerLevel, currentLevel));
        }
    }

    public void UpgradeNow()
    {
        BigBomber.cash -= UpgradeCost;
        PDATA.IncreasePowerUpLevel(type);
    }
}

public enum UpgradeType
{
    MAGAZINE_SIZE,
    DAMAGE_MULT,
    BOUNTY_MULT,
    AMMO_REGEN,
}