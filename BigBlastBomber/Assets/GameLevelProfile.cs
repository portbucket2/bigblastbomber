﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLevelProfile : ScriptableObject
{

    public AreaController initialArea;
    public List<AreaController> areaPrefabs;
    public Color groundColor;

}