﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelProfile
{


    public const float BASE_BUDGET = 150;
    public const float BUDGET_MULT = 1.21f;
    public const float HP_GROWTH = 1.08f;
    public const float EXPECTED_MAGAZINE_RATIO = 0.5f;
    public const float STRESS_REDUCTION = 0.75f;
    public const float BIG_MULT = 0.65f;
    public const float SMALL_MULT = 0.08f;

    public int levelIndex;
    public float powerBudget;
    public float enemyHPMult = 1;

    public float ammoThresholdBudgetStress;
    //public int bigAmmoBoxSize;
    //public int smallAmmoBoxSize;

    public LevelProfile(int levelIndex)
    {
        this.levelIndex = levelIndex;
        powerBudget = BASE_BUDGET * Mathf.Pow(BUDGET_MULT, levelIndex);
        enemyHPMult = Mathf.Pow(HP_GROWTH, levelIndex);

        float expectedMagSize = powerBudget * EXPECTED_MAGAZINE_RATIO;
        ammoThresholdBudgetStress = expectedMagSize* STRESS_REDUCTION;
        Debug.LogFormat("Expected Budget = {0}", expectedMagSize);

        //bigAmmoBoxSize = Mathf.CeilToInt(expectedMagSize * BIG_MULT);
        //smallAmmoBoxSize = Mathf.CeilToInt(expectedMagSize * SMALL_MULT);
    }

}
