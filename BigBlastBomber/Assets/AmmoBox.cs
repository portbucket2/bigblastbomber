﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoBox : MonoBehaviour
{
    public int ammoCount;

    internal EnemyAgentScript agent;

    public GameObject bigVisual;
    public GameObject smallVisual;

    public void InitAmmo(int ammo, bool isBig)
    {
        ammoCount = ammo;
        if (ammo <= 0)
        {
            FishSpace.Pool.Destroy(this.gameObject);
        }
        bigVisual.SetActive(isBig);
        smallVisual.SetActive(!isBig);
    }
}
