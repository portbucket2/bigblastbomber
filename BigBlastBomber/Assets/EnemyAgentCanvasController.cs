﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EnemyAgentCanvasController : MonoBehaviour
{
    public GameObject hp_object;
    public GameObject rewardObject;

    public Text cashRewardText;
    public Text ammoRewardText;
    public Image hpBar;


    EnemyAgentScript agent;
    private void Init()
    {
        agent = transform.parent.GetComponent<EnemyAgentScript>();
        if(!agent)Debug.Log("Agent not found");
        agent.onHPChange += OnHpChange;
        agent.onInit += OnInit;
    }
    private void OnEnable()
    {
        if (agent == null) Init();
    }

    private void OnInit()
    {
        rewardObject.SetActive(false);
        hp_object.SetActive(true);
        OnHpChange();
    }

    private void OnHpChange()
    {
        float hpRatio = agent.HP_current / agent.HP_pool;
        if (hpRatio > 0)
        {
            hpBar.fillAmount = hpRatio;
        }
        else
        {
            rewardObject.SetActive(true);
            hp_object.SetActive(false);

            if (agent.ammoBounty > 0)
            {
                ammoRewardText.text = string.Format("+{0} Ammo", agent.ammoBounty);
            }
            else
            {
                ammoRewardText.text = "";
            }
            if (agent.cashBounty > 0)
            {
                cashRewardText.text = string.Format("+{0} Cash", agent.cashBounty);
            }
            else
            {
                cashRewardText.text = "";
            }
        }
    }


}
