﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crawler : MonoBehaviour
{
    public static Transform crawlingTransform;
    public bool isMain = false;
    public Vector3 crawlVec;
    // Start is called before the first frame update
    void Start()
    {
        if (isMain) crawlingTransform = transform;
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(crawlVec * Time.deltaTime);
    }
}
