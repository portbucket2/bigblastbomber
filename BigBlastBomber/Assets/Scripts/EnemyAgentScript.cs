﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyAgentScript : MonoBehaviour//, IEvilAgent
{



    public bool hasCompletedItsPurpose;
    public float baseHpToCashMult = 0.334f;
    public float baseHPPool = 15;
    public Transform blastRadiusTrans;

    public const bool becomeUntargetableWhenDamageInvested = true;
    

    public int cashBounty { get; protected set; }
    public int ammoBounty
    {
        get
        {
            if (ammoRef==null)
                return 0;
            return ammoRef.ammoCount;
        }
    }
    public float HP_current { get; protected set; }
    public float HP_pool { get; protected set; }

    public List<float> inboundDamages = new List<float>();
    public float registeredDmg
    {
        get
        {
            float total=0;
            foreach (float item in inboundDamages)
            {
                total += item;
            }
            return total;
        }
    }
    public float currentDamage
    {
        get
        {
            return Mathf.Clamp(HP_pool-HP_current, 0, HP_pool);
        }
    }

    public bool IsDead { get; protected set; }
    void OnDisable()
    {
        CompleteYourPurpose(true);
    }


    public virtual bool IsUntergetable
    {
        get
        {
            if (!becomeUntargetableWhenDamageInvested) return IsDead;
            else return (registeredDmg >= HP_pool);
        }
    }
    public bool IsOutOfRange
    {
        get
        {
            return this.transform.position.z <= BigBomber.instance.minRangeRef.position.z || this.transform.position.z >= BigBomber.instance.maxRangeRef.position.z;
        }
    }

    public  virtual void CompleteYourPurpose(bool report)
    {
        if (hasCompletedItsPurpose) return;

        hasCompletedItsPurpose = true;
        SetSelected(false);
        DisableBlastRadius();
       if(report)EnemySpwner.instance.ReportEnemyRecievedJudgement(this, Mathf.Clamp(registeredDmg, 0, HP_pool));
    }

    public AmmoBox ammoRef;

    public Transform scalableVisuals;

    public GameObject targetObject;

    public event System.Action onInit;
    public event System.Action onHPChange;
    public float AnalyzeHPPower(float powerFactor)
    {
        if (ammoBounty <= 0)
            return baseHPPool * powerFactor;
        else
            return 0;
    }
    public float Initialize(float powerFactor)
    {
        hasCompletedItsPurpose = false;
        IsDead = false;
        if (ammoRef==null)
        {
            cashBounty = Mathf.CeilToInt(baseHPPool*baseHpToCashMult*BigBomber.cashMultiplier);
        }

        SetSelected(false);
        scalableVisuals.gameObject.SetActive(true);

        HP_pool = baseHPPool * powerFactor;
        HP_current = HP_pool;
        inboundDamages.Clear();
        targetObject.SetActive(false);
        DisableBlastRadius();
        onInit?.Invoke();

        if (ammoBounty<=0)
            return HP_pool;
        else
            return 0;
    }

    public void SetSelected(bool selected)
    {
        targetObject.SetActive(selected);
    }

    public ParticleSystem deathParticle;
    public ParticleSystem bigHitParticle;
    public ParticleSystem hitParticle;
    public virtual void TakeDamage(float dmg,bool isBigHit)
    {
        HP_current -= dmg;
        ChangeHPEventTrigger();
        if (HP_current <= 0 && !IsDead)
        {
            IsDead = true;
            BigBomber.cash += cashBounty;
            BigBomber.instance.currentAmmo += ammoBounty;
            BigBomber.instance.score += Mathf.CeilToInt( HP_pool);
            scalableVisuals.gameObject.SetActive(false);
            deathParticle.Play();
        }
        else
        {
            if (isBigHit) bigHitParticle.Play();
            else hitParticle.Play();
        }
    }
    public void ChangeHPEventTrigger()
    {
        onHPChange?.Invoke();
    }



    public virtual void SetBlastRadius(float forDmage)
    {
        if (IsDead) {
            DisableBlastRadius();
            return;
        }
        blastRadiusTrans.gameObject.SetActive(true);
        blastRadiusTrans.localScale = Vector3.one*Mathf.Sqrt( (registeredDmg + forDmage) / HP_pool);
    }
    public void DisableBlastRadius()
    {
        blastRadiusTrans.gameObject.SetActive(false);
    }

    public virtual void RegisterDamageInvestment(float damage)
    {
        inboundDamages.Add(damage);
    }

}

//public interface IEvilAgent
//{
//    float Initialize(float powerFactor);
//}