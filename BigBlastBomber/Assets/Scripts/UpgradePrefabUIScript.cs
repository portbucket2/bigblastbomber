﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradePrefabUIScript : MonoBehaviour
{

    public UpgradeProfile profile;
    public string postfix;
    public Button button;
    public Text titleText;
    public Text currentValue;
    public Text incrementValue;
    public Text cost;
    public Text currentLevel;

    public void Load(System.Action onClickCallback)
    {
        titleText.text = string.Format("{0}", profile.title);
        currentLevel.text = string.Format("lvl {0}", profile.currentLevel);

        currentValue.text = string.Format("{0}{1}", profile.CurrentValue,postfix);
        incrementValue.text = string.Format("+{0}", profile.UpgradeGain);
        cost.text = string.Format("Cost: {0}", profile.UpgradeCost);

        button.interactable = profile.UpgradeCost<=BigBomber.cash;
        if (button.interactable)
        {
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(()=> {

                profile.UpgradeNow();
                onClickCallback?.Invoke();
            });
        }
    }
}
