﻿using UnityEngine;
using System.Collections;

public static class TimeString 
{
    public static string FromSeconds_0toMAX(float seconds)
    {
            int sec = Mathf.RoundToInt(Mathf.Clamp(seconds, 0, float.MaxValue));
            int min = sec / 60;
            sec = (sec - (min * 60));
            return string.Format("{0}:{1:D2}", min, sec);
    }
}
