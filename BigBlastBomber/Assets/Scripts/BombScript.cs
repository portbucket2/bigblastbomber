﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishSpace;

public class BombScript : MonoBehaviour
{

    public float travelSpeedMax = 12;
    public float travelSpeedMin = 5;
    public Transform fuelTank;

    public float fullPowerTime = 1.0f;
    public float warmUpTime = 0.2f;
    public Vector3 maxScale;
    public Vector3 minScale;

    public int minDmg = 1;
    public int maxDmg = 25;
    int currentDamage { get { return (int)(Mathf.Lerp(minDmg, maxDmg, fill)*BigBomber.damageMultiplier); } }
    float fill;
    float bombInitTime;
    bool isBig;
    public int Init(EnemyAgentScript target)
    {
        isBig = false;
        this.target = target;
        fill = 0;
        fuelTank.localScale = minScale;
        bombInitTime = Time.realtimeSinceStartup;
        target.SetBlastRadius(currentDamage);

        return currentDamage;
    }

    public int Pump()
    {
        if (Time.realtimeSinceStartup < bombInitTime + warmUpTime) return 0;
        isBig = true;
        int oldDmg = currentDamage;

        float progress = Mathf.Clamp01((Time.realtimeSinceStartup - bombInitTime-warmUpTime) / fullPowerTime);
        fill = progress;
        fuelTank.localScale = Vector3.Lerp(minScale,maxScale,Mathf.Clamp01(progress));
        target.SetBlastRadius(currentDamage);

        return currentDamage - oldDmg;
    }

    public EnemyAgentScript target;
    float travelStartTime;
    float timeBudget;
    public void Release()
    {

        Vector2 selfV = new Vector2(this.transform.position.x,this.transform.position.z);
        Vector2 targV = new Vector2(target.transform.position.x, target.transform.position.z);


        target.RegisterDamageInvestment(currentDamage);
        timeBudget = Vector2.Distance(selfV,targV) / Mathf.Lerp(travelSpeedMax, travelSpeedMin, fill);
        travelStartTime = Time.time;
        StartCoroutine(ReleaseCo());
    }

    IEnumerator ReleaseCo()
    {
        this.transform.SetParent(EnemySpwner.instance.creationParentTrans);
        Vector3 initialPosition = this.transform.localPosition;
        Vector3 endPosition = target.transform.localPosition+target.transform.parent.localPosition;// new Vector3(target.transform.localPosition.x,this.transform.localPosition.y,target.transform.localPosition.z) ;
        Vector3 travelDirVec = (endPosition - initialPosition).normalized;
        while (Time.time < travelStartTime + timeBudget)
        {
            float progress = Mathf.Clamp01((Time.time - travelStartTime) / timeBudget);
            this.transform.localPosition = Vector3.Lerp(initialPosition,endPosition, progress);

            Vector3 targetDirVec = Vector3.Lerp(travelDirVec,Vector3.down,progress);
            float dirLerpRate = Mathf.Lerp(2, 0.2f, progress);
            this.transform.forward = Vector3.Lerp(this.transform.forward, targetDirVec, dirLerpRate*Time.deltaTime);
            yield return null;
        }

        target.TakeDamage( currentDamage,isBig);
        this.target = null;
        Pool.Destroy(this.gameObject);
        StopAllCoroutines();
    }

    //private void Update()
    //{
    //    if (target != null)
    //    {
    //        this.transform.
    //    }
    //}

    //public void Explode()
    //{
    //}

}
