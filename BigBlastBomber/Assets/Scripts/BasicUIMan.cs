﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BasicUIMan : MonoBehaviour
{
    public static BasicUIMan instance;

    public Button levelStartButton;
    public Image ammoBar;
    public Image progressBar;
    public Text ammoText;
    public Text cashText1;
    public Text cashText2;
    public Text gameLevelText;
    public Text tutorial0Text;
    public Text lowAmmoWarningText;
    public Text countDownText;

    public Text scoreText;
    public Text highScoreText;

    public Text currentLevelText;
    public Text nextLevelText;
    private void Awake()
    {
        instance = this;
        lowAmmoTextCol = lowAmmoWarningText.color;
    }

    // Start is called before the first frame update
    private void Start()
    {

        BigBomber.instance.onAmmoChanged += UpdateAmmoData;
        EnemySpwner.instance.onLevelProgressChanged += UpdateLevelProgress;

        BigBomber.instance.onScoreChanged += UpdateScoreText;

        BigBomber.onCashValueChanged +=  UpdateCash;
        BigBomber.onGameLevelChanged += UpdateLevelText;
        LoadTransition();

        levelStartButton.onClick.AddListener(LoadLevel);

        UpdateCash(BigBomber.cash);
        UpdateLevelText(BigBomber.gameLevel);
        UpdateScoreText();
    }
    private void OnDestroy()
    {
        BigBomber.onCashValueChanged -= UpdateCash;
        BigBomber.onGameLevelChanged -= UpdateLevelText;
    }

    private void UpdateLevelProgress(float progress)
    {
        progressBar.fillAmount = progress;
    }
    private void UpdateCash(int currentCash)
    {
        cashText1.text = string.Format("Cash: {0}",currentCash);
        cashText2.text = string.Format("Cash: {0}", currentCash);
    }
    private void UpdateLevelText(int level)
    {
        gameLevelText.text = string.Format("Enter War Zone {0}", level+1);
    }
    private void UpdateAmmoData(int now,int max)
    {
       //ammoBar.fillAmount = (now*1.0f) / (max);
        ammoText.text = string.Format("{0}/{1}",now,max);
        
    }
    private void UpdateScoreText()
    {
        scoreText.text = string.Format("Score:\n{0}",BigBomber.instance.score);
        highScoreText.text = string.Format("Best:\n{0}", BigBomber.instance.highScoreHD.value);
    }
    void Update()
    {
        ammoBar.fillAmount = BigBomber.instance.GetAmmoBarRatio();
    }

    public GameObject transitionObject;
    public GameObject gameRunningObject;
    public List<UpgradePrefabUIScript> upgradePrefabUIScripts;

    void LoadTransition()
    {
        transitionObject.SetActive(true);
        gameRunningObject.SetActive(false);
        foreach (UpgradePrefabUIScript item in upgradePrefabUIScripts)
        {
            item.Load(LoadTransition);
        }

    }

    void LoadLevel()
    {
        StartCoroutine(LoadLevelRoutine());
    }
    IEnumerator LoadLevelRoutine()
    {
        currentLevelText.text = (BigBomber.gameLevel+1).ToString();
        nextLevelText.text = (BigBomber.gameLevel + 2).ToString();
        levelStartButton.gameObject.SetActive(false);
        countDownText.gameObject.SetActive(true);
        for (int i = 3; i >0; i--)
        {
            countDownText.text = i.ToString();
            yield return new WaitForSeconds(1);
        }
        levelStartButton.gameObject.SetActive(true);
        countDownText.gameObject.SetActive(false);

        transitionObject.SetActive(false);
        gameRunningObject.SetActive(true);
        BigBomber.instance.OnLevelStart();
        EnemySpwner.instance.NewLevelBudget(new LevelProfile(BigBomber.gameLevel));
    }

    public GameObject successObj;
    public GameObject failureObj;

    public bool lastlevelFailed = true;
    public void LevelEnd(bool success)
    {
        lastlevelFailed = !success;
        BigBomber.instance.fastFly = true;
        LoadTransition();
        successObj.SetActive(success);
        failureObj.SetActive(!success);

    }

    public void DeleteAllPlayerPref()
    {
        PlayerPrefs.DeleteAll();
        FishSpace.HardDataCleaner.Clean();
        Application.LoadLevel(Application.loadedLevelName);
    }
    public void Restart()
    {
        //Application.LoadLevel(Application.loadedLevelName);
        
        EnemySpwner.instance.FailedToFinishOffTarget();
        BigBomber.instance.currentTarget = null;
    }


    Color lowAmmoTextCol;
    Coroutine lowAmmoRoutine;
    public void ShowLowAmmoWarning()
    {
        if (lowAmmoRoutine!=null) StopCoroutine(lowAmmoRoutine);
        lowAmmoRoutine =  StartCoroutine(lowAmmoWarningCoroutine());
    }

    public IEnumerator lowAmmoWarningCoroutine()
    {
        float startTime = Time.realtimeSinceStartup;
        float allotedTime = 0.25f;
        while (Time.realtimeSinceStartup < startTime + allotedTime)
        {
            float lerpVal = Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / allotedTime);
           lowAmmoWarningText.color = new Color(lowAmmoTextCol.r, lowAmmoTextCol.g, lowAmmoTextCol.b, lerpVal);
            yield return null;
        }
        yield return new WaitForSeconds(.5f);
        startTime = Time.realtimeSinceStartup;
        allotedTime = 0.25f;
        while (Time.realtimeSinceStartup < startTime + allotedTime)
        {
            float lerpVal = Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / allotedTime);
            lowAmmoWarningText.color = new Color(lowAmmoTextCol.r, lowAmmoTextCol.g, lowAmmoTextCol.b, 1 - lerpVal);
            yield return null;
        }
    }
}
