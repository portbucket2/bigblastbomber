﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AreaController : MonoBehaviour
{
    public AmmoBox ammoBox;

    public List<EnemyAgentScript> enemies;
    public float areaLength=10;

    public GroundChooser gc;

    bool initialized = false;
    public void OneTimeInit()
    {

        if (!initialized)
        {
            enemies.Clear();
            foreach (Transform tr in this.transform)
            {
                EnemyAgentScript eas = tr.GetComponent<EnemyAgentScript>();
                if (eas)
                {
                    enemies.Add(eas);
                    AmmoBox ammo = eas.GetComponent<AmmoBox>();
                    if (ammo != null)
                    {
                        ammoBox = ammo;
                    }
                }
            }

            enemies.Sort((x, y) => x.transform.position.z.CompareTo(y.transform.position.z));
            initialized = true;
        }
    }

    public float Initialize(float powerFactor, bool groundChoice)
    {
        OneTimeInit();
        
        gc.Init(groundChoice);

        float totalPower = 0;
        foreach (var enemy in enemies)
        {
           totalPower+= enemy.Initialize(powerFactor);
        }
        return totalPower;
    }
    public float CostAnalyze(float powerFactor)
    {
        OneTimeInit();
        float totalPower = 0;
        foreach (var enemy in enemies)
        {
            totalPower += enemy.AnalyzeHPPower(powerFactor);
        }
        return totalPower;
    }

}
