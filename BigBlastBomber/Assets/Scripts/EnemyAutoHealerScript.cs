﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyAutoHealerScript : EnemyAgentScript//, IEvilAgent
{
    public override void TakeDamage(float dmg, bool isBigHit)
    {
        float possibleDmgedHP = HP_pool - dmg;


        SetBlastRadius(0);
        if (possibleDmgedHP <= 0 && !IsDead)
        {
            HP_current =0;
            ChangeHPEventTrigger();

            IsDead = true;
            BigBomber.cash += cashBounty;
            BigBomber.instance.currentAmmo += ammoBounty;
            BigBomber.instance.score += Mathf.CeilToInt(HP_pool);
            scalableVisuals.gameObject.SetActive(false);
            deathParticle.Play();
        }
        else
        {
            HP_current = Mathf.Min(0.02f, HP_current - dmg);
            ChangeHPEventTrigger();

            if (isBigHit) bigHitParticle.Play();
            else hitParticle.Play();
        }
    }

    public override bool IsUntergetable
    {
        get
        {
            if (!becomeUntargetableWhenDamageInvested) return IsDead;
            else
            {
                foreach (float dmg in inboundDamages)
                {
                    if (dmg >= HP_pool) return true;
                }
                return false;
            }
        }
    }

    public override void SetBlastRadius(float dmg)
    {
        if (IsDead)
        {
            DisableBlastRadius();
            return;
        }
        blastRadiusTrans.gameObject.SetActive(true);
        blastRadiusTrans.localScale = Vector3.one * Mathf.Sqrt(( dmg) / HP_pool);
    }
    public override void  RegisterDamageInvestment(float damage)
    {
        inboundDamages.Clear();
        inboundDamages.Add(damage);
    }


    public float visualRegenRate=10;
    private void Update()
    {
        if (IsDead) return;
        HP_current += (HP_pool * visualRegenRate * Time.deltaTime);
        ChangeHPEventTrigger();
    }
}

