﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishSpace;

public class PDATA
{
    private static PDATA _pdata;

    public static PDATA pdata
    {
        get
        {
            if (_pdata == null) _pdata = new PDATA();
            return _pdata;
        }
    }


    private HardData<int> highScoreHD;
    private HardData<int> ordinaryBalanceHD;
    private HardData<int> gameLevelProgressHD;
    //private HardData<int> premiumBalanceHD;

    //private HardData<int> selectedHeroHD;
    private Dictionary<UpgradeType, HardData<int>> powerUpLevelsHD = new Dictionary<UpgradeType, HardData<int>>();
    //private Dictionary<MadHeroID, HardData<bool>> heroUnlockStatusHD = new Dictionary<MadHeroID, HardData<bool>>();



   // private HardData<int> goalSetIndexHD;
    //private List<HardData<bool>> currentGoalStatusList = new List<HardData<bool>>();


    //public static int GoalSet_GetCurentIndex()
    //{
    //    return pdata.goalSetIndexHD.value;
    //}
    //public static bool GoalSet_ReadyToReward()
    //{
    //    foreach (var goalCopmplete in pdata.currentGoalStatusList)
    //    {
    //        if (!goalCopmplete.value) return false;
    //    }
    //    return true;
    //}
    //public static void GoalSet_MoveToNext()
    //{
    //    BMadAnalyticsHandler.ReportGoalComplete(pdata.goalSetIndexHD.value, BMadAnalyticsHandler.Labels_PlayerGoals.CLAIMED);
    //    pdata.goalSetIndexHD.value += 1;
    //    foreach (var item in pdata.currentGoalStatusList)
    //    {
    //        item.value = false;
    //    }
    //}
    //public static bool Goal_IsComplete(int goalIndex)
    //{
    //    if (goalIndex < 0 || goalIndex >= pdata.currentGoalStatusList.Count) throw new System.Exception("Goal index is out of bound");
    //    return pdata.currentGoalStatusList[goalIndex].value;
    //}
    //public static void Goal_ReportComplete(int goalIndex)
    //{
    //    if (goalIndex < 0 || goalIndex >= pdata.currentGoalStatusList.Count) throw new System.Exception("Goal index is out of bound");
    //    if (pdata.currentGoalStatusList[goalIndex].value) return;
    //    else
    //    {
    //        pdata.currentGoalStatusList[goalIndex].value = true;
    //        switch (goalIndex)
    //        {
    //            case 0:
    //                BMadAnalyticsHandler.ReportGoalComplete(pdata.goalSetIndexHD.value, BMadAnalyticsHandler.Labels_PlayerGoals.COMPLETED_A);
    //                break;
    //            case 1:
    //                BMadAnalyticsHandler.ReportGoalComplete(pdata.goalSetIndexHD.value, BMadAnalyticsHandler.Labels_PlayerGoals.COMPLETED_B);
    //                break;
    //            case 2:
    //                BMadAnalyticsHandler.ReportGoalComplete(pdata.goalSetIndexHD.value, BMadAnalyticsHandler.Labels_PlayerGoals.COMPLETED_C);
    //                break;
    //        }
    //    }
    //}



    private PDATA()
    {
        highScoreHD = new HardData<int>("HIGH_SCORE", 0);
        ordinaryBalanceHD = new HardData<int>("ORDINARY_BALANCE", 0);
        gameLevelProgressHD = new HardData<int>("GAME_LEVEL_PROGRESS", 0);
        //premiumBalanceHD = new HardData<int>("PREMIUM_BALANCE", 0);


        foreach (UpgradeType ptype in System.Enum.GetValues(typeof(UpgradeType)))
        {
            powerUpLevelsHD.Add(ptype, new HardData<int>(string.Format("PLEVEL_{0}", ptype), 0));
        }

        //selectedHeroHD = new HardData<int>("SELECTED_HERO", 0);
        //foreach (MadHeroID mhtype in System.Enum.GetValues(typeof(MadHeroID)))
        //{
        //    bool isUnlockedFromStart = (((int)mhtype) == 0);
        //    heroUnlockStatusHD.Add(mhtype, new HardData<bool>(string.Format("PLEVEL_{0}", mhtype), isUnlockedFromStart));
        //}

        //goalSetIndexHD = new HardData<int>("GOALSET_INDEX", 0);
        //for (int i = 0; i < GoalManager.GOALS_PER_SET; i++)
        //{
        //    currentGoalStatusList.Add(new HardData<bool>(string.Format("GOAL_STATE_{0}", i), false));
        //}
    }

    public static int GetPowerUpLevel(UpgradeType ptype)
    {
        HardData<int> plevelHData = pdata.powerUpLevelsHD[ptype];
        return plevelHData.value;
    }
    public static void IncreasePowerUpLevel(UpgradeType ptype)
    {
        HardData<int> plevelHData = pdata.powerUpLevelsHD[ptype];
        plevelHData.value = plevelHData.value + 1;
    }
    public static void ResetAllPowerUpLevels()
    {
        foreach (var item in pdata.powerUpLevelsHD)
        {
            item.Value.value = 0;
        }
    }

    //public static MadHeroID currentlySelectedHero
    //{
    //    get
    //    {
    //        return (MadHeroID)pdata.selectedHeroHD.value;
    //    }
    //    set
    //    {
    //        pdata.selectedHeroHD.value = (int)value;
    //    }
    //}


    //public static bool IsUnlocked(MadHeroID mhID)
    //{
    //    return pdata.heroUnlockStatusHD[mhID].value;
    //}
    //public static void UnlockNow(MadHeroID mhID)
    //{
    //    pdata.heroUnlockStatusHD[mhID].value = true;
    //}


    public static int highScore
    {
        get
        {
            return pdata.highScoreHD.value;
        }
        set
        {
            pdata.highScoreHD.value = value;
        }
    }
    public static int ordinaryBalance
    {
        get
        {
            return pdata.ordinaryBalanceHD.value;
        }
        set
        {
            pdata.ordinaryBalanceHD.value = value;
        }
    }
    public static int gameLevel
    {
        get
        {
            return pdata.gameLevelProgressHD.value;
        }
        set
        {
            pdata.gameLevelProgressHD.value = value;
        }
    }
    //public static int premiumBalance
    //{
    //    get
    //    {
    //        return pdata.premiumBalanceHD.value;
    //    }
    //    set
    //    {
    //        pdata.premiumBalanceHD.value = value;
    //    }
    //}

}



