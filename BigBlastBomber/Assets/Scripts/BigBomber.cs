﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishSpace;

public class BigBomber : MonoBehaviour
{


    public HardData<int> highScoreHD;
    private int _score;
    public int score
    {
        get
        {
            return _score;
        }
        set
        {
            _score = value;
            if (_score >= highScoreHD.value)
            {
                highScoreHD.value = _score;
                onHighScoreChanged?.Invoke();
            }
            onScoreChanged?.Invoke();
        }
    }
    public event System.Action onScoreChanged;
    public event System.Action onHighScoreChanged;

    public static System.Action<int> onCashValueChanged;
    public static int cash
    {
        get { return PDATA.ordinaryBalance; }
        set
        {
            int oldValue = PDATA.ordinaryBalance;
            PDATA.ordinaryBalance = value;

            if (oldValue != value)
                onCashValueChanged?.Invoke(value);
        }
    }
    public bool fastFly=true;
    public static System.Action<int> onGameLevelChanged;
    public static int gameLevel
    {
        get { return PDATA.gameLevel; }
        set
        {
            PDATA.gameLevel = value;
            onGameLevelChanged?.Invoke(value);
        }
    }


    public int ammoMagazineSize { get; private set; }

    public event System.Action<int,int> onAmmoChanged;
    public int _currentAmmo;
    public int currentAmmo {
        get
        {
            return _currentAmmo;
        }
        set
        {
            int oldValue = _currentAmmo;
            _currentAmmo = Mathf.Clamp( value,0,ammoMagazineSize);
            if (oldValue != _currentAmmo)
                onAmmoChanged?.Invoke(_currentAmmo, ammoMagazineSize);
        }
    }


    public static float cashMultiplier
    {
        get
        {
            return instance.bountyUpgradeProfile.CurrentValue/100.0f;
        }
    }
    public static float damageMultiplier
    {
        get
        {
            return instance.damageUpgradeProfile.CurrentValue / 100.0f;
        }
    }

    public UpgradeProfile bountyUpgradeProfile;
    public UpgradeProfile damageUpgradeProfile;
    public UpgradeProfile magazineUpgradeProfile;
    public UpgradeProfile ammoRefillRateProfile;

    public void OnLevelStart()
    {
        ammoMagazineSize = magazineUpgradeProfile.CurrentValue;
        currentAmmo = magazineUpgradeProfile.CurrentValue;
        if(BasicUIMan.instance.lastlevelFailed)score = 0;
    }

    public static BigBomber instance;
    public Transform minRangeRef ;
    public Transform maxRangeRef ;


    public EnemyAgentScript currentTarget;

    private void Awake()
    {
        instance = this;
        currentAmmo = ammoMagazineSize;
        highScoreHD = new HardData<int>("HIGHSCORE",0);
    }

    public float GetAmmoBarRatio()
    {
        return (refill+currentAmmo)/ ammoMagazineSize;
    }
    public float refill;
    private void Update()
    {
        if (EnemySpwner.instance.currentLevelProfile == null) return;

        if (currentTarget != null && (!currentTarget.IsUntergetable) && (currentTarget.IsOutOfRange))
        {
            EnemySpwner.instance.FailedToFinishOffTarget();
            currentTarget = null;
            return;
        }
        //if(currentTarget)Debug.Log(currentTarget.IsUntergetable);
        if (currentTarget == null || currentTarget.IsUntergetable || currentTarget.IsOutOfRange)
        {

            SetCurrentTarget(EnemySpwner.instance.GetNextEnemy());
            
        }

        if(currentAmmo<ammoMagazineSize)
            refill += ammoRefillRateProfile.CurrentValue*Time.deltaTime / 60.0f;

        //Debug.Log(ammoRefillRateProfile.currentLevel);

        if (refill > 0)
        {
             currentAmmo += (int)refill;
            refill -= (int)refill;
        }

        if (Input.GetMouseButtonDown(0))
        {
            CreateBomb();
        }
        else if (Input.GetMouseButton(0))
        {
            PumpBomb();
        }
        else if(Input.GetMouseButtonUp(0))
        {
            ReleaseBomb();
        }
    }

    public GameObject bombPrefab;
    public Transform bombSpwnPointLeft;
    public Transform bombSpwnPointRight;
    private BombScript bomb;

    private void CreateBomb()
    {
        if (currentTarget == null) return;
        if (currentTarget.ammoBounty <= 0 && currentAmmo <= 0) return;

        Transform spwnParent = currentTarget.transform.position.x > 0 ? bombSpwnPointRight : bombSpwnPointLeft;
        bomb = Pool.Instantiate(bombPrefab, spwnParent).GetComponent<BombScript>();
        bomb.transform.localPosition = Vector3.zero;
        bomb.transform.localRotation = Quaternion.identity;
         if(currentTarget.ammoBounty <= 0) currentAmmo -= bomb.Init(currentTarget);
        else bomb.Init(currentTarget);
    }
    private void PumpBomb()
    {
        if (bomb)
        {
            if (currentTarget == null)
            {
                ReleaseBomb();
                return;
            }
            else
            {
                if (!bomb.target) Debug.Log("bomb no target");
                if (currentTarget.ammoBounty <= 0)
                {
                    currentAmmo -= bomb.Pump();
                    if (currentAmmo <= 0)
                    {
                        ReleaseBomb();
                        BasicUIMan.instance.ShowLowAmmoWarning();
                    }
                }
                else bomb.Pump();
            }

        }
    }
    private void ReleaseBomb()
    {
        if (bomb)
        {
            bomb.transform.parent = null;
            bomb.Release();
            bomb = null;
        }
    }


    public void SetCurrentTarget(EnemyAgentScript eas)
    {
        if (currentTarget)
        {
            if (bomb)
            {
                //Debug.Log("BombReleasedWithout player auth");
                ReleaseBomb();
            }


            currentTarget.CompleteYourPurpose(true);
        }
        if (eas != null) fastFly = false;
        currentTarget = eas;

        if (currentTarget) currentTarget.SetSelected(true);
    }
}

