﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptableCreator 
{
    [UnityEditor.MenuItem("Assets/Create/UpgradeProfile")]
    public static void UpgradeProfile()
    {
        UpgradeProfile so = ScriptableObject.CreateInstance<UpgradeProfile>();
        UnityEditor.AssetDatabase.CreateAsset(so, "Assets/UpgradeProfile.asset");
        UnityEditor.AssetDatabase.SaveAssets();
        UnityEditor.EditorUtility.FocusProjectWindow();
        UnityEditor.Selection.activeObject = so;
    }

    [UnityEditor.MenuItem("Assets/Create/GameLevelProfile")]
    public static void GameLevelProfile()
    {
        GameLevelProfile so = ScriptableObject.CreateInstance<GameLevelProfile>();
        UnityEditor.AssetDatabase.CreateAsset(so, "Assets/GameLevelProfile.asset");
        UnityEditor.AssetDatabase.SaveAssets();
        UnityEditor.EditorUtility.FocusProjectWindow();
        UnityEditor.Selection.activeObject = so;
    }
}



