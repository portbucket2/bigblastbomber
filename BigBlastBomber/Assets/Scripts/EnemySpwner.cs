﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishSpace;

public class EnemySpwner : MonoBehaviour
{
    public static EnemySpwner instance;
    public event System.Action<float> onLevelProgressChanged;
    public float flySpeed = 2;
    public float fastFlySpeed = 6;
    public float accLerpSpeed = 0.5f;
    public float deccLerpSpeed = 1.5f;
    public float discardDepth;
    public Transform creationPositionTrans;
    public Transform creationParentTrans;
    public List<GameObject> prefabs;
    public List<GameObject> dummyPrefabs;

    List<AreaController> createdAreas = new List<AreaController>();


    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        zOffset = -15;

        CreateNext();
        CreateNext();
        CreateNext();

        currentFlySpeed = flySpeed;
    }


    float currentFlySpeed;
    private void Update()
    {
        currentFlySpeed = Mathf.Lerp(currentFlySpeed, ((BigBomber.instance.fastFly) ? fastFlySpeed : flySpeed), ((BigBomber.instance.currentTarget != null) ? deccLerpSpeed : accLerpSpeed) * Time.deltaTime);
        creationParentTrans.Translate(0, 0, -  currentFlySpeed* Time.deltaTime);

        if (createdAreas[0].transform.position.z < -discardDepth)
        {
            Pool.Destroy(createdAreas[0].gameObject);
            createdAreas.RemoveAt(0);
            CreateNext();
        }
    }

    //public float ammoThresholdStress;
    //public float powerDepletionStress;
    //public float powerBudget=0;
    //public float powerMult=1;
    public float zOffset=0;

    bool groundChoice;




    public LevelProfile currentLevelProfile;
    public List<AreaController> currentAreasOnSchedule = new List<AreaController>();

    public List<GameLevelProfile> gameLevelProfiles = new List<GameLevelProfile>();
    public GameLevelProfile generalGameLevelProfile;

    float powerDepletionStress;
    List<AreaController> areaPrefabs;
    public void NewLevelBudget(LevelProfile levelProfile)
    {
        currentLevelProfile = levelProfile;
        enemiesCurrent.Clear();
        playerPowerUtilized = 0;
        powerDepletionStress = 0;
        onLevelProgressChanged?.Invoke(0);
        //powerBudget = levelProfile.powerBudget;

        currentAreasOnSchedule.Clear();
        float levelPowerCreated = 0;


        GameLevelProfile glp;
        if (levelProfile.levelIndex >= 0 && levelProfile.levelIndex < gameLevelProfiles.Count)
        {
            glp = gameLevelProfiles[levelProfile.levelIndex];
        }
        else
        {
            glp = generalGameLevelProfile;
        }

        if (glp.initialArea != null)
        {
            AreaController nextArea = glp.initialArea;
            currentAreasOnSchedule.Add(nextArea);
            levelPowerCreated += nextArea.CostAnalyze(levelProfile.enemyHPMult);
        }
        while (levelPowerCreated < levelProfile.powerBudget)
        {
            AreaController nextArea = glp.areaPrefabs[Random.Range(0, glp.areaPrefabs.Count)];
            currentAreasOnSchedule.Add(nextArea);
            levelPowerCreated += nextArea.CostAnalyze(levelProfile.enemyHPMult);
        }
    }

    public void FailedToFinishOffTarget()
    {
        Debug.Log("Game Failed");
        if(currentLevelProfile!=null)
            OnLevelComplete(false);
    }

    public void ReportEnemyRecievedJudgement(EnemyAgentScript enemy, float dmgDone)
    {
        if (enemiesCurrent.Count <= 0) Debug.LogError("EnemyCountMismatch");

        if (enemiesCurrent.Contains(enemy)) enemiesCurrent.Remove(enemy);
        playerPowerUtilized += dmgDone;
        if (currentLevelProfile != null)
        {
            float playerSuccessRatio = playerPowerUtilized / currentLevelProfile.powerBudget;
            onLevelProgressChanged?.Invoke(playerSuccessRatio);

            if (enemiesCurrent.Count == 0)
            {
                BigBomber.gameLevel++;
                OnLevelComplete(true);
            }

        }


    }
    public float winThreshold = 0.8f;

    public void OnLevelComplete(bool success)
    {
        currentLevelProfile = null;
        for (int i = enemiesCurrent.Count-1; i >=0 ; i--)
        {

            if (enemiesCurrent[i]) enemiesCurrent[i].CompleteYourPurpose(false);
            else enemiesCurrent.RemoveAt(i);
        }
        enemiesCurrent.Clear();
        currentAreasOnSchedule.Clear();
        BasicUIMan.instance.LevelEnd(success);
    }

    public void CreateNext()
    {
        GameObject go;
        AreaController area;
        if (currentAreasOnSchedule.Count > 0)
        {
            AreaController nextAreaPref = currentAreasOnSchedule[0];
            currentAreasOnSchedule.Remove(nextAreaPref);

            go = Pool.Instantiate(nextAreaPref.gameObject, creationParentTrans);
            area = go.GetComponent<AreaController>();

            groundChoice = !groundChoice;
            float areaPower = area.Initialize(currentLevelProfile.enemyHPMult, groundChoice);

            enemiesCurrent.AddRange(area.enemies);

            powerDepletionStress += areaPower;

            int areaAmmoBoxSize;
            bool isBig;
            if (powerDepletionStress > currentLevelProfile.ammoThresholdBudgetStress)
            {
                areaAmmoBoxSize = Mathf.CeilToInt(BigBomber.instance.ammoMagazineSize * LevelProfile.BIG_MULT);
                isBig = true;
            }
            else
            {
                areaAmmoBoxSize = Mathf.CeilToInt(BigBomber.instance.ammoMagazineSize * LevelProfile.SMALL_MULT);
                isBig = false;
            }
            area.ammoBox.InitAmmo(areaAmmoBoxSize, isBig);
            powerDepletionStress -= areaAmmoBoxSize;
        }
        else
        {
            go = Pool.Instantiate(dummyPrefabs[Random.Range(0, dummyPrefabs.Count)], creationParentTrans);
            area = go.GetComponent<AreaController>();

        }
        float length = area.areaLength;
        zOffset += (length * 0.5f);
        go.transform.localPosition = new Vector3(0, 0, zOffset);
        zOffset += (length * 0.5f);
        createdAreas.Add(area);
    }
    /*
    public void CreateNext()
    {
        if (powerBudget > 0)
        {
            int unlockedAreaCount = Mathf.Min(prefabs.Count, BigBomber.gameLevel+3);


            GameObject enemy = Pool.Instantiate(prefabs[Random.Range(0, unlockedAreaCount)], creationParentTrans);
            AreaController area = enemy.GetComponent<AreaController>();
            float length = area.areaLength;
            zOffset += (length * 0.5f);
            enemy.transform.localPosition =  new Vector3(0, 0, zOffset);
            zOffset += (length * 0.5f);
            createdAreas.Add(area);
            groundChoice = !groundChoice;
            float areaPower = area.Initialize(currentLevelProfile.enemyHPMult,groundChoice);
            powerBudget -= areaPower;
            levelPowerCreated += areaPower;
            enemiesCurrent.AddRange( area.enemies);

            powerDepletionStress += areaPower;

            int areaAmmoBoxSize;
            bool isBig;
            if (powerDepletionStress > currentLevelProfile.ammoThresholdBudgetStress)
            {
                areaAmmoBoxSize = Mathf.CeilToInt(BigBomber.instance.ammoMagazineSize*LevelProfile.BIG_MULT);
                isBig = true;
            }
            else
            {
                areaAmmoBoxSize = Mathf.CeilToInt(BigBomber.instance.ammoMagazineSize * LevelProfile.SMALL_MULT);
                isBig = false;
            }
            area.ammoBox.InitAmmo(areaAmmoBoxSize,isBig);
            powerDepletionStress -= areaAmmoBoxSize;
        }
        else
        {
            GameObject enemy = Pool.Instantiate(dummyPrefabs[Random.Range(0, dummyPrefabs.Count)],creationParentTrans);
            AreaController area = enemy.GetComponent<AreaController>();
            float length = area.areaLength;
            zOffset += (length * 0.5f);
            enemy.transform.localPosition =  new Vector3(0, 0, zOffset);
            zOffset += (length * 0.5f);
            createdAreas.Add(area);
        }
    }
    */
    public EnemyAgentScript GetNextEnemy()
    {
        foreach (var area in createdAreas)
        {
            foreach (var enemy in area.enemies)
            {
                if (enemy.IsUntergetable) continue;
                if (enemy.IsOutOfRange) continue;
                return enemy;
            }
        }
        return null;
    }

    //public float levelPowerCreated;
    //public int levelEnemyCreated;
    public List<EnemyAgentScript> enemiesCurrent;
    public float playerPowerUtilized;

    



    //public List<GameObject> scheduledLevelAreas = new List<GameObject>();
    //public void LoadUpLevel(LevelProfile levelPro)
    //{
    //    createdPower = 0;
    //    while (createdPower < levelPro.powerBudget)
    //    {
    //        GameObject prefab = prefabs[Random.Range(0, prefabs.Count)];

    //    }
    //}


    //public List<AreaController> createdAreaList = new List<AreaController>();

    //float createdPower;
    //void Create(float powerTarget)
    //{
    //    createdPower = 0;
    //    zOffset = 0;
    //    while (createdPower<powerTarget)
    //    {
    //        GameObject enemy = Pool.Instantiate(prefabs[Random.Range(0, prefabs.Count)], creationParentTrans);
    //        AreaController area = enemy.GetComponent<AreaController>();
    //        float length = area.areaLength;
    //        zOffset += (length*0.5f);
    //        enemy.transform.position = creationPositionTrans.position + new Vector3(0, 0, zOffset);
    //        zOffset += (length * 0.5f);
    //        createdAreaList.Add(area);
    //        //enemy.transform.position = creationPositionTrans.position + new Vector3(Random.Range(xMin, xMax), 0, 0);
    //        createdPower += area.Initialize(1);//Random.Range(scaleMin, scaleMax));
    //    }
    //}

    //IEnumerator Clean()
    //{
    //    while (true)
    //    {

    //        yield return new WaitForSeconds(0.2f);

    //        for (int i = createdAreaList.Count - 1; i >= 0; i--)
    //        {
    //            AreaController item = createdAreaList[i];
    //            if (item.transform.position.z < -10)
    //            {
    //                Pool.Destroy(item.gameObject);
    //                createdAreaList.RemoveAt(i);
    //            }
    //        }
    //    }

    //}



}
