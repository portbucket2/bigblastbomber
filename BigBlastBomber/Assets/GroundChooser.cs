﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundChooser : MonoBehaviour
{
    public GameObject groundA;
    public GameObject groundB;
    // Start is called before the first frame update
    public void Init(bool groundChoice)
    {
        groundA.SetActive(groundChoice);
        groundB.SetActive(!groundChoice);
    }

}
