﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishSpace;

public class Tutorial0 : MonoBehaviour
{

    public EnemyAgentScript tutorialAgent;
    static HardData<bool> tutorialDone;

    public Color col;

    static Tutorial0 instance;
    // Start is called before the first frame update
    void Start()
    {
        if (instance != null)
        {
            this.enabled = false;
            return;
        }

        instance = this;
        if(tutorialDone==null) tutorialDone = new HardData<bool>("Tutorial0", false);
        if (!tutorialDone.value) StartCoroutine(StartNow());
        col = BasicUIMan.instance.tutorial0Text.color;
        BasicUIMan.instance.tutorial0Text.color = new Color(0,0,0,0);
        BasicUIMan.instance.tutorial0Text.gameObject.SetActive(true);
    }
    bool timeFreezingdone;
    // Update is called once per frame
    //void Update()
    //{
    //    if (tutorialDone.value)
    //    {
    //        this.enabled = false;
    //        return;
    //    }
    //    else
    //    {
    //        if (this.transform.position.z < 5)
    //        {
    //            if (this.tutorialAgent == BigBomber.instance.currentTarget && !timeFreezingdone)
    //            {
    //                Time.timeScale = 0.05f;

    //                timeFreezingdone = true;
    //            }
    //            if (timeFreezingdone)
    //            {
    //                if (this.tutorialAgent != BigBomber.instance.currentTarget)
    //                {
    //                    Time.timeScale = 1;
    //                    tutorialDone.value = true;
    //                }
    //            }
    //        }
    //    }
    //}

    IEnumerator StartNow()
    {
        while (this.transform.position.z > 5 || !(this.tutorialAgent == BigBomber.instance.currentTarget))
        {
            yield return null;
        }

        float startTime = Time.realtimeSinceStartup;
        float allotedTime = 0.4f;
        while (Time.realtimeSinceStartup<startTime+allotedTime)
        {
            float lerpVal = Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / allotedTime);
            Time.timeScale = Mathf.Lerp(1, 0.1f,lerpVal);
            BasicUIMan.instance.tutorial0Text.color = new Color(col.r,col.g,col.b,lerpVal);
            yield return null;
        }

        while (this.tutorialAgent == BigBomber.instance.currentTarget)
        {
            yield return null;
        }
        tutorialDone.value = true;
        startTime = Time.realtimeSinceStartup;
        allotedTime = 0.1f;
        while (Time.realtimeSinceStartup < startTime + allotedTime)
        {
            float lerpVal = Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / allotedTime);
            Time.timeScale = Mathf.Lerp(0.1f, 1, lerpVal);
            BasicUIMan.instance.tutorial0Text.color = new Color(col.r, col.g, col.b, 1- lerpVal);
            yield return null;
        }
        Time.timeScale = 1;

    }
}
